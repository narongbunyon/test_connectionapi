<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;


class CommentController extends Controller
{
    //
    function commentList (){
        // cdd in data//
        // dd(555);
        $data= Http::get('https://jsonplaceholder.typicode.com/comments')->json();

        return view('comment',['data'=>$data]);
    }
}
