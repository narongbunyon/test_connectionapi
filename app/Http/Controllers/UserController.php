<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class UserController extends Controller
{
    //
    function userLists(){
        $users= Http::get('https://jsonplaceholder.typicode.com/users')->json();

        return view('user',['users'=>$users]);
    }
}




