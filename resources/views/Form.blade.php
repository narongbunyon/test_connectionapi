<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>form-create</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" />
    <script src="https://unpkg.com/axios/dist/axios.min.js" defer></script>
</head>

<body>

    <div class="creation" id="app-form">
        <form class="acitives w-25 d-f m-5">
            <!-- Email input -->
            <div class="form-outline mb-4">
                <label class="form-label" for="form1Example1">Email address</label>
                <input id="form1Example1" class="form-control" v-model="inputname" />
            </div>
            <!-- Password input -->
            <div class="form-outline mb-4">
                <label class="form-label" for="form1Example2">Password</label>
                <input type="password" id="form1Example2" v-model="inputPass" class="form-control" />
            </div>
            <!-- 2 column grid layout for inline styling -->
            <div class="row mb-4">
                <div class="col d-flex justify-content-center">
                    <!-- Checkbox -->
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" value="password" id="form1Example3" checked />
                        <label class="form-check-label" for="form1Example3"> Remember me </label>
                    </div>
                </div>
                <div class="col">
                    <!-- Simple link -->
                    <a href="#!">Forgot password?</a>
                </div>
            </div>
            <!-- Submit button -->
            <button type="button" class="btn btn-primary btn-block" @click="creatarrays">Create</button>
            <div class="resutl">
                <p>@{{inputname}}</p>
                <p>@{{inputPass}}</p>
            </div>
        </form>


        <div class="array"> Array- here : @{{myarray}}</div>
        <!-- --------------------- -->
    </div>

    <script src="{{ mix('js/app.js') }}"></script>
    <script src="{{ mix('js/main/create.js') }}"></script>

</body>

</html>
