<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>testing</title>
    <link rel="stylesheet"href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"/>
    <script src="https://unpkg.com/axios/dist/axios.min.js" defer></script>
    <style>


    #app{
        justify-content: center;
        align-items: center;
        display: flex;
        margin:10px auto 10px;
    }

        .containers{
            justify-content: center;
            align-items: center;
            margin: auto;
        }

        .images{
            margin: auto;
            justify-content: center;
            align-items: center;
            background: teal;
            margin: 20px 20px;
            border-radius: 2px;
        }
        .pic{
            display: flex;
            margin:10px auto 10px;
            align-items: center;
            justify-content: center;
            width: 20%;
        }
        img{
            width: 100%;
        }
        .names{
            display: flex;
            margin: 5px 5px auto;
            justify-content: center;
            align-items: center;

        }
        .mails{
            display: flex;
            justify-content: center;
            align-items: center;
            margin: auto;
        }

    </style>
</head>
<body>

    <div id="app">
            <form class="acitives w-25 d-f m-5">
                <!-- Email input -->
                <div class="form-outline mb-4">
                    <label class="form-label" for="form1Example1">Email address</label>
                    <input type="email" id="form1Example1" class="form-control" />
                </div>
                <!-- Password input -->
                <div class="form-outline mb-4">
                    <label class="form-label" for="form1Example2">Password</label>
                    <input type="password" id="form1Example2" class="form-control" />
                </div>
                <!-- 2 column grid layout for inline styling -->
                <div class="row mb-4">
                    <div class="col d-flex justify-content-center">
                    <!-- Checkbox -->
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" value="" id="form1Example3" checked />
                        <label class="form-check-label" for="form1Example3"> Remember me </label>
                    </div>
                    </div>
                    <div class="col">
                    <!-- Simple link -->
                    <a href="#!">Forgot password?</a>
                    </div>
                </div>
                <!-- Submit button -->
                <button type="submit" class="btn btn-primary btn-block">Create</button>
        </form>

        <div class="owner">
            <div class="containers">
                <div class="user-formate"  v-for="(user, index) in users" :key="index">
                    <div class="images">
                        <div class="pic">
                            <img  :src="user.avatar" class="rounded-circle" width="50" />
                        </div>
                        <div class="names">
                            <div class="full-name">Full names : @{{user.first_name}}  @{{user.last_name}}</div>
                        </div>
                        <p class="mails">Email @{{user.email}}</p>
                    </div>
                </div>
            </div>
         </div>

    </div>

    <script src="{{ mix('js/app.js') }}"></script>
    <script src="{{ mix('js/main/users.js') }}"></script>

</body>
</html>
