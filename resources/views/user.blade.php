<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>user</title>
    <link rel="stylesheet"href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"/>

    <style>

        .containers{
            margin: 10px 10px 10px 10px;
            display: flex;
        }

        .text-show{
            display: flex;
            text-align: center;
            justify-content: center;
            margin: 20px 20px;
            font-family: sans-serif;
            font-size: 20px;
        }

        .users{
            width: 50%;
            background:skyblue;
            margin: 10px 10px;

        }
        .names{
            margin: 10px 10px;

        }

        
    </style>



</head>
<body>




    <div  class="containers">

            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-xl-5 col-md-8 border rounded">
                        <div class="text-show">Please complete your information!</div>
                        <form class="bg-white  rounded-5 shadow-5-strong p-5">
                        <!-- Email input -->
                        <div class="form-outline mb-4">
                        <input type="email" id="form1Example1" class="form-control" />
                        <label class="form-label" for="form1Example1">Email address</label>
                        </div>

                        <!-- Password input -->
                        <div class="form-outline mb-4">
                            <input type="password" id="form1Example2" class="form-control" />
                        <label class="form-label" for="form1Example2">Password</label>
                        </div>

                        <!-- 2 column grid layout for inline styling -->
                        <div class="row mb-4">
                        <div class="col d-flex justify-content-center">
                            <!-- Checkbox -->
                            <div class="form-check">
                            <input class="form-check-input" type="checkbox" value="" id="form1Example3" checked />
                            <label class="form-check-label" for="form1Example3">
                                Remember me
                            </label>
                            </div>
                        </div>

                        <div class="col text-center">
                            <!-- Simple link -->
                            <a href="#!">Forgot password?</a>
                        </div>
                        </div>

                        <!-- Submit button -->
                        <button type="submit" class="btn btn-primary btn-block">Sign in</button>
                    </form>
                </div>
            </div>
        </div>

        <div class="users">
            <h1>Here is getting Users</h1>
                <ul>
                    @foreach($users as $userlist)
                    <li class="names">Name : {{print_r($userlist['name'])}}</li>
                    @endforeach
                </ul>
            </div>

        </div>
</body>
</html>
