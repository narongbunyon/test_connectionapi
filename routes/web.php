<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Here is the rout the conplete that for Controlller with views------------------


Route::get('/test', function () {
    return view('testing');
});


Route::get('/form', function () {
    return view('Form');
});








///Here is the way that we connection with rout controllers
Route::get('/user',"UserController@userLists");

Route::get('/comment',"CommentController@commentList");
